<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 10/01/2017
 * Time: 15:53
 */

namespace giftbox\utils;


use giftbox\models\Client;

class Authentification
{
    public static function createUser($email, $password, $passwordConfirm)
    {
        if ($password != null && $password === $passwordConfirm && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $tmp = Client::where('mail', '=', $email)->first();
            if ($tmp == NULL) {

                $hash = password_hash($password, PASSWORD_DEFAULT);

                $client = new Client();
                $client->mail = $email;
                $client->nom = 'pierre';
                $client->mdp = $hash;
                $client->role_id = 1;
                $client->save();
            } else {
                throw new AuthException('existe deja');
            }

        } else {
            throw new AuthException('bad login');
        }
    }

    public static function authenticate($email, $password)
    {
// charger utilisateur $user
// vérifier $user->hash == hash($password)
// charger profil ($user->id)
        $client = Client::where('mail', '=', $email)->first();
        if ($client != NULL) {

            $hash = $client->mdp;
            if (password_verify($password, $hash)) {
                self::loadProfile($client->id);
            } else {
                throw new AuthException('bad login');
            }
        } else {
            throw new AuthException('bad login1');
        }
    }

    private static function loadProfile($uid)
    {
// charger l'utilisateur et ses droits
// détruire la variable de session
// créer variable de session = profil chargé
        $client = Client::where('id', '=', $uid)->first();
        $profile = array('user_mail' => $client->mail,
            'userid' => $client->id,
            'role_id' => $client->role->id,
            'auth_level' => $client->role->auth_level);
        $_SESSION['profile'] = $profile;
    }

    public static function checkAccessRights($required)
    {
        if (isset($_SESSION['profile'])) {
            if ($_SESSION['profile']['auth_level'] < $required)
                throw new AuthException("access denied");
        }
        else{
            throw new AuthException("access denied");
        }
    }

}