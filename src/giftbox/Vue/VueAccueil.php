<?php

namespace giftbox\Vue;

class VueAccueil
{

    private $content;

    function __construct()
    {

}
    public function render($id)
    {
        $app= \Slim\Slim::getInstance();

        //initialisation des routes
        $urlA=$app->urlFor('accueil');
        $urlP=$app->urlFor('pre');
        $urlC=$app->urlFor('cat');
        $urlCof=$app->urlFor('listerCoffret');
        $urlCo=$app->urlFor('connexion');
        $chemin = "web/css";

        switch ($id){
            case 1:
                $chemin = "web/css";
                $cont = $this->Accueil();
                break;
            case 2:
                $cont=$this->connexion();
                $chemin = "web/css";
                break;
            case 3:
                $cont=$this->inscription();
                $chemin="web/css";
                break;
            case 4:
                $cont=$this->validation();
                $chemin="web/css";
                break;
            case 5:
                $cont=$this->validationE();
                $chemin="web/css";
            break;
            case 6:
                $cont=$this->Payement();
                $chemin="web/css";
                break;
        }
        //methode qui cree le contenue
        $SL="";
        $req = \giftbox\models\Categorie::select()->get();
        foreach ($req as $item){
            $urlC1=$app->urlFor('catId',['idCat'=>$item->id]);
            $SL=$SL.<<<end
            <li><a href='$urlC1'>$item->nom</a></li>

end;
        }
        $Connexion="CONNEXION";
        if(isset($_SESSION["profile"])){
            $Connexion="MON COMPTE";
            $urlCo=$app->urlFor('MonCompte');
        }

        //la page html
        $html = <<<END
                <!DOCTYPE html>
                <html lang="fr">
                    <head>
                        <title>GiftBox</title>
                        <meta charset="utf-8">
                        <link rel="stylesheet" href="$chemin/homeGB.css">   
                    </head>
                    <body>
                        <header><a href='$urlA'><img src="$chemin/box_logo.png" alt="logoGiftBox"></a></header>
                        <nav>
                            <ul id="menu">
                                <li class="linav"><a href='$urlA'>HOME</a></li>
                                <li class="linav"><a href='$urlC'>CATEGORIE</a>
                            <ul>
                                $SL
                            </ul></li>
                                <li class="linav"><a href='$urlP'>PRESTATION</a></li>
                                <li class="linav"><a href="$urlCof">COFFRET</a></li>
                                <li class="linav"><a href='$urlCo'>$Connexion</a></li>
                            </ul>
                        </nav>                    
                        <div>
                            $cont
                        </div>                 
                        <footer> <h1>Giftbox maj 2016</h1>
                            <section class="Foot">
                                <ul>
                                    <li>A PROPOS :</li>
                                    <li><a href="#">- Condition générales de ventes</a></li>
                                    <li><a href="#">- Données personnelles</a></li>
                                    <li><a href="#">- mentions legale</a></li>
                                    <li><a href="#">- cookies</a></li>
                                </ul>
                            </section>
                            <section class="Foot">
                                <ul>
                                    <li>Le groupe GIFTBOX :</li>
                                    <li><a href="#">- qui somme nous?</a></li>
                                    <li><a href="#">- recrutement</a></li>
                                </ul>
                            </section>
                            <section class="Foot">
                                <ul>
                                    <li>AIDE, SAV ET SERVICE :</li>
                                    <li><a href="#">- SAV</a></li>
                                    <li><a href="#">- besoin d'aide</a></li>
                                </ul>
                            </section>
                        </footer>
                    </body>
                </html>
END;


        return $html;
    }

    public function Accueil(){

        //initialisation du contenu
        $cont=<<<END
        <section>
            <H1> Bienvenue sur Giftbox, le site E-commerce qui permet de realiser vos réves ou se de vos amis</h1>
                <p> nous vous offrons de nombreuse prestation de differentes categorie tel que herbergement, des repas,des activitée et des activité dangereuse.</p>
                <p>pour cela il faut choisir au moins deux prestation de qui ne font pas partie de la meme categorie pour pouboir valider votre coffret.</P>
        </section>
END;
        //on retourne le message
        return $cont;

    }

    public function connexion(){
        $app=\Slim\Slim::getInstance();
        $urlINs=$app->urlFor("inscription");
        $urlCon=$app->urlFor("seCo");
        //initialisation du contenu
        $cont=<<<END
        <section class="connexion">
            <form id='form1' method='POST' action="$urlCon">
                   <div class="fromIP">
                   <H1>CONNEXION</h1>
                        <div class="form-group">
                        <input type="email" name="email" placeholder="  giftbox@contact.fr">
                        </div>
                        <div class="form-group">
                        <input type="password" name="password" placeholder="  Password">
                        </div>
                        <div class="form-group">
                        <p><a href="#">mot passe oublié</a></p>
                        </div>
                        <div class="form-group">
                          <button type='submit'>Connexion</button>
                        </div>
                        <div class="form-group">
                          <p><a href="$urlINs">inscription</a></p>
                        </div>
                   </div>
                </form>
        </section>
END;

        return $cont;

    }

    public function inscription(){
        $app=\Slim\Slim::getInstance();
        $urlCO=$app->urlFor("connexion");
        $urlIns=$app->urlFor("sinscrire");
        $cont=<<<END
        <div class="login-box">
            <form id='form1' method='POST' action="$urlIns">
                <div class="login">
                <P>EMAIL</p>
                 <input type="Email" name="email" placeholder="giftbox@contact.fr">
                </div>
                <div class="login">
                <P>MOT DE PASSE</p>
                    <input type="password" name="password" placeholder="mot de passe">
                </div>
                <div class="login">
                    <P>CONFIRMER LE MOT DE PASSE</p>
                    <input type="password" name="confirm" placeholder="mot de passe">
                </div>
                <a href="$urlCO">Annuler</a>
                <button type="submit" class="B">
                    <span class="f-button-content">Créer un compte</span>
                    <span class="f-button-progress"></span>
                </button>
            </form>
        </div>

END;
    return $cont;
    }

    public function Validation(){
        $app=\Slim\Slim::getInstance();
        $urlR=$app->urlFor("recapCoffret");

        $cont=<<<END
        <h1>Valider votre Coffret</h1>
        <div class="login-box">
            <form id='form1' method='POST' action="$urlR">
                <div class="loginN">
                <P>Nom</p>
                 <input type="text" name="nom" placeholder="  Nom">
                </div>
                <div class="loginN">
                <P>Prenom</p>
                    <input type="text" name="prenom" placeholder="  Prenom">
                </div>
                <div class="login">
                    <P>Mail</p>
                    <input type="email" name="email" placeholder="  giftbox@contact.fr">
                </div>
                <div class="login">
                <P>Message</p>
                    <textarea tabindex="8" rows="7" name="message" style="width: 100%;"></textarea>
                </div>
                <div class="login">
                <P>Type de payement :</p>
                    <label>Payement classique<label><input type="radio" name="groupe-radio1"  value=1 checked >
                    <label>Cagnote<label><input type="radio" name="groupe-radio1"  value=1>
                </div>
                <a href="#">Annuler</a>
                <button type="submit" class="B">
                    <span class="f-button-content">Valider</span>
                    <span class="f-button-progress"></span>
                </button>
            </form>
        </div>

END;
        return $cont;
    }

    public function ValidationE(){
        $app=\Slim\Slim::getInstance();
        $urlP=$app->urlFor("pre");
        $cont=<<<END
        <div class="login-box">
            <h1>erreur :</h1><h3>il y a moins de deux prestation dans votre coffret</h3><h1>ou
            </h1><h3>il y a que des prestations de la meme categorie(petit rappelle 
            il faut au moins un article de deux categories differenetes</h3>
           <form id='form1' method='GET' action="$urlP">
            <button type="submit" class="B">
                    <span class="f-button-content">Continuer vos achats</span>
                    <span class="f-button-progress"></span>
                </button></form>
                
        </div>

END;
        return $cont;
    }
    function Payement(){
        $app=\Slim\Slim::getInstance();
        $urlP=$app->urlFor("pre");
        $urlCurl=$app->urlFor("CreeUrl");

        $cont=<<<END
        <div class="login-box">
            <h1>Merci de votre achat</h1>
            <p>pour créer l'url cadeau clicker sur le lien:</p>
             <a href="$urlCurl">Ma Giftbox</a>
        </div>
END;
        return $cont;
    }
}