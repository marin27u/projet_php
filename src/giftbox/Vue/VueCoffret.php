<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 02/01/2017
 * Time: 12:07
 */

namespace giftbox\Vue;


class VueCoffret
{

    private $content;


    function __construct($object=NULL)
    {
        $this->content = $object;

    }


    function render($id_vue)
    {
        //message au cas ou les methodes ne renvoie pas de resultat
        $cont="<p>erreur</p>";

        $app= \Slim\Slim::getInstance();

        //initialisation des routes
        $urlA=$app->urlFor('accueil');
        $urlP=$app->urlFor('pre');
        $urlC=$app->urlFor('cat');
        $urlCof=$app->urlFor('listerCoffret');
        $urlCo=$app->urlFor('connexion');

        //methode qui cree le contenue
        switch ($id_vue) {
            case 1 :
                $chemin="web/css";
                $css="AllPreGB";
                $cont=$this->listerCoffret();
                break;
            case 2 :
                $chemin="web/css";
                $css="AllPreGB";
                $cont=$this->RecapitlatifCoffret();
                break;
            case 3 :
                $chemin="../web/css";
                $css="AllPreGB";
                $cont=$this->coffretUrl();
                break;
            case 4 :
                $chemin="web/css";
                $css="AllPreGB";
                $cont=$this->MonCompte();
                break;
        }

        $SL="";
        $req = \giftbox\models\Categorie::select()->get();
        foreach ($req as $item) {
            $urlC1 = $app->urlFor('catId', ['idCat' => $item->id]);
            $SL = $SL . <<<end
            <li><a href='$urlC1'>$item->nom</a></li>
            
end;
        }
        $Connexion="CONNEXION";
        if(isset($_SESSION["profile"])){
            $Connexion="MON COMPTE";
            $urlCo=$app->urlFor('MonCompte');
        }

        //page HTML
        $html =<<<END
            <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <title>GiftBox</title>
                    <meta charset="utf-8">
		            <link rel="stylesheet" href="$chemin/$css.css">
                </head>
                <body>
	
	                <header><a href='$urlA'><img src="$chemin/box_logo.png" alt="logoGiftBox"></a></header>
		
		            <nav> 
			            <ul id="menu">
				            <li class="linav"><a href='$urlA'>HOME</a></li>
				            <li class="linav"><a href='$urlC'>CATEGORIE</a>
				        <ul>
                            $SL
                        </ul></li>
				            <li class="linav"><a href='$urlP'>PRESTATION</a></li>
				            <li class="linav"><a href='$urlCof'>COFFRET</a></li>
				            <li class="linav"><a href='$urlCo'>$Connexion</a></li>
			            </ul>
		            </nav>
		            
		            <div class='pre'> 
			            $cont
		            </div> 
		
		            <footer> <h1>Giftbox maj 2016</h1> 
		                <section class="Foot">
		                    <ul>
			                    <li>A PROPOS :</li>
			                    <li><a href="#">- Condition générales de ventes</a></li>
			                    <li><a href="#">- Données personnelles</a></li>
			                    <li><a href="#">- mentions legale</a></li>
			                    <li><a href="#">- cookies</a></li>
		                    </ul>
		                </section>
		
		                <section class="Foot">
		                    <ul>
			                    <li>Le groupe GIFTBOX :</li>
			                    <li><a href="#">- qui somme nous?</a></li>
			                    <li><a href="#">- recrutement</a></li>
		                    </ul>
		                </section>
		
		                <section class="Foot">
		                    <ul>
			                    <li>AIDE, SAV ET SERVICE :</li>
			                    <li><a href="#">- SAV</a></li>
			                    <li><a href="#">- besoin d'aide</a></li>
		                    </ul>
		                </section>
		            </footer>
	            </body>
	
            </html>
END;


        return $html;
    }



    function listerCoffret()
    {


        $cont="";
        // on recuprer l'instance de slim pour crée des liens qui passeron par l'index
        $app= \Slim\Slim::getInstance();
        $urlV=$app->urlFor('validation');

        foreach ($this->content as $item) {

            $Q=$item["quantite"];
            $P=$item["prestation"];
            //initialisation des routes qui mene a l'affichage du produit
            $url = $app->urlFor('preId', ['id' => $P->id]);
            $ajout = $app->urlFor('ajoutCoffret', ['id' => $P->id]);
            $supp = $app->urlFor('suppCoffret', ['id' => $P->id]);
            $prix = "<p>" . $P->prix . "€</p>";
            //initialisation du contenu
            $cont = $cont . <<<End
            <img class='pic' src='web/css/pic_1.png' alt=''>
            <section id=section1>
                <div class='prod'>
                   <a href='$url'><img src = 'web/img/$P->img' alt = '$P->img'/></a>
                </div>
            
                <div class='detail'>
                    <div>
                        <p><a href='$url'>$P->nom</a></p>
                         <p>$P->descr</p>
                         <p>$Q</p>
                    </div>

                    <div class='prix'>
                        $prix
                         <form id='form1' method='GET' action="$ajout">
                        
                        <button type='submit'>ajouter</button>
                        </form>
                        <form id='form1' method='GET' action="$supp">
                        
                        <button type='submit'>Supprimer</button>
                        </form>
                        
                     </div>
                        
                </div>
            </section>
End;
            }


            $cont = $cont . "<img class='pic' src='web/css/pic_1.png' alt=''>";
            $prix = $_SESSION['prixPanier'];
            $cont = $cont . <<<End
            <fieldset style='margin: 5% 20% 0% 20%'> 
                <legend>Montant total:</legend>
                <p>$prix €</p>
                <form id='form1' method='GET' action="$urlV">
                    <button type='submit'>Valider Coffret</button>
                </form>
            </fieldset>

End;
            return $cont;

    }


    function RecapitlatifCoffret(){

        $cont = "<h1>recapitulatif de votre Coffret</h1><div>";
        // on recuprer l'instance de slim pour crée des liens qui passeron par l'index
        $app = \Slim\Slim::getInstance();
        $urlPay=$app->urlFor("Payement");
        $i=0;
        foreach ($this->content as $item) {
            $i++;
            $Q=$item["quantite"];
            $P=$item["prestation"];
            $prix = $P->prix."€";
            $cont = $cont . <<<End
            <section id="section1">
            <fieldset style='margin: 5% 10% 0% 10%'> 
                <legend>Prestation n°$i</legend>
                <div class='imR'>
                    <img src = 'web/img/$P->img' alt = '$P->img'/></a>
                </div>
                <div class='desc'>
                    <h3>$P->nom</h3>
                    <p>Quantité :$Q</p>
                    <p>Prix :$prix</p>
                 </div>
            </fieldset>
</section>
End;
        }
        $prix = $_SESSION['prixPanier'];
        $cont = $cont . <<<End
            <fieldset style='margin: 5% 10% 0% 10%'> 
                <legend>Montant total:</legend>
                <p>$prix €</p>
                <form id='form1' method='GET' action="$urlPay">
                    <button type='submit'>Payer Coffret</button>
                </form>
            </fieldset>

End;
        return $cont."</section></div>";
    }

    function coffretUrl(){

        $cont = "<h1>votre coffret </h1><div>";
        $cont = $cont."<p>!!!!!!Attention!!!!!!</p><p>gardez precieusement cette url</p>";
        // on recuprer l'instance de slim pour crée des liens qui passeron par l'index
        $app = \Slim\Slim::getInstance();
        $m=$this->content["message"];
        $cont = $cont . <<<End
            <fieldset style='margin: 5% 10% 0% 10%'>
            <legend>Message</legend> 
                      <h1>$m</h1>
            </fieldset>
End;
        unset($this->content["message"]);
        foreach ($this->content as $item) {
            $Q=$item["quantite"];
            $P=$item["prestation"];
            $cont = $cont . <<<End
            <section id="section1">
            <fieldset style='margin: 5% 10% 0% 10%'> 
                <legend></legend>
                <div class='imR'>
                    <img src = '../web/img/$P->img' alt = '$P->img'/></a>
                </div>
                <div class='desc'>
                    <h3>$P->nom</h3>
                    <p>Quantité :$Q</p>
                 </div>
            </fieldset>
</section>
End;
        }
        $prix = $_SESSION['prixPanier'];
        $cont = $cont . <<<End
            <fieldset style='margin: 5% 10% 0% 10%'> 
                     <h1>GiftBox vous souhaite un agreable moment de dégustation, de sensation et d'émotion </h1>
            </fieldset>

End;
        return $cont."</section></div>";
    }

    function MonCompte(){
        $cont="<h2>Mon Compte</h2>";
        $i=0;
        $app = \Slim\Slim::getInstance();
        $dec=$app->urlFor("deconnexion");
        foreach ($this->content as $url){
            $U = $app->urlFor('CoffretUrl', ['id' => $url->code]);
            $P="nonPayer.jpg";
            $i++;
            if($url->etat=="payé") {
                $P="payer.jpg";
            }
            $cont = $cont . <<<End
            <fieldset style='margin: 5% 10% 0% 10%'> 
                    <legend>Coffret </legend>
                    <p>Url coffret :<a href="$U">$U</a></p>
                    <p><img src = 'web/img/$P' alt = '$P'/>etat : $url->etat</p>
            </fieldset>
            

End;
        }
        $cont = $cont . <<<End
        <form id='form1' method='GET' action="$dec">
                <button type='submit'>Deconnexion</button>
            </form>
End;
        return $cont;
    }

}