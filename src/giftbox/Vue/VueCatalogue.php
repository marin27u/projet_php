<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 15/12/2016
 * Time: 10:59
 */
namespace giftbox\Vue;

class VueCatalogue
{


    private $content;


    function __construct($object)
    {
        $this->content = $object;
    }
    public function render($id_vue) {

        //message au cas ou les methodes ne renvoie pas de resultat
        $cont="<p>erreur</p>";

        $app= \Slim\Slim::getInstance();

        //initialisation des routes
        $urlA=$app->urlFor('accueil');
        $urlP=$app->urlFor('pre');
        $urlC=$app->urlFor('cat');
        $urlCof=$app->urlFor('listerCoffret');
        $urlCo=$app->urlFor('connexion');

        //methode qui cree le contenue
        switch ($id_vue) {
            case 1 :
                $chemin="web/css";
                $css="AllPreGB";
                $cont=$this->listerPrestations();
                break;
            case 2 :
                $chemin="../web/css";
                $css="preGB";
                $cont=$this->afficherPrestation();
                break;
            case 3 :
                $chemin="web/css";
                $css="Categorie";
                $cont=$this->afficherCategorie();
                break;
            case 4 :
                $chemin="../web/css";
                $css="AllPreGB";
                $cont=$this->afficherCategoriePrestation();
                break;
            case 5 :
                $chemin="../web/css";
                $css="AllPreGB";
                $cont=$this->listerPrestations(2);
                break;
            case 6 :
                $chemin="../web/css";
                $css="AllPreGB";
                $cont=$this->afficherCategoriePrestation();
                break;
        }
        $SL="";
        $req = \giftbox\models\Categorie::select()->get();
        foreach ($req as $item) {
            $urlC1 = $app->urlFor('catId', ['idCat' => $item->id]);
            $SL = $SL . <<<end
            <li><a href='$urlC1'>$item->nom</a></li>

end;
        }
        $Connexion="CONNEXION";
        if(isset($_SESSION["profile"])){
            $Connexion="MON COMPTE";
            $urlCo=$app->urlFor('MonCompte');
        }

        //page HTML
       $html =<<<END
            <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <title>GiftBox</title>
                    <meta charset="utf-8">
		            <link rel="stylesheet" href="$chemin/$css.css">
                </head>
                <body>
	
	                <header><a href='$urlA'><img src="$chemin/box_logo.png" alt="logoGiftBox"></a></header>
		
		            <nav> 
			            <ul id="menu">
				            <li class="linav"><a href='$urlA'>HOME</a></li>
				            <li class="linav"><a href='$urlC'>CATEGORIE</a>
				        <ul>
                            $SL
                        </ul></li>
				            <li class="linav"><a href='$urlP'>PRESTATION</a></li>
				            <li class="linav"><a href='$urlCof'>COFFRET</a></li>
				            <li class="linav"><a href='$urlCo'>$Connexion</a></li>
			            </ul>
		            </nav>
		            
		            <div class='pre'> 
			            $cont
		            </div> 
		
		            <footer> 
		            <h1>Giftbox maj 2016</h1> 
		                <section class="Foot">
		                    <ul>
			                    <li>A PROPOS :</li>
			                    <li><a href="#">- Condition générales de ventes</a></li>
			                    <li><a href="#">- Données personnelles</a></li>
			                    <li><a href="#">- mentions legale</a></li>
			                    <li><a href="#">- cookies</a></li>
		                    </ul>
		                </section>
		
		                <section class="Foot">
		                    <ul>
			                    <li>Le groupe GIFTBOX :</li>
			                    <li><a href="#">- qui somme nous?</a></li>
			                    <li><a href="#">- recrutement</a></li>
		                    </ul>
		                </section>
		
		                <section class="Foot">
		                    <ul>
			                    <li>AIDE, SAV ET SERVICE :</li>
			                    <li><a href="#">- SAV</a></li>
			                    <li><a href="#">- besoin d'aide</a></li>
		                    </ul>
		                </section>
		            </footer>
	            </body>
            </html>
END;


                return $html;
                }


    private function listerPrestations($id = 1)
    {
        if($id==1){
            $chemin="web";
        }else{
            $chemin="../Web";
        }
        $cont="";
        // on recuprer l'instance de slim pour crée des liens qui passeron par l'index
        $app= \Slim\Slim::getInstance();
        $urlP=$app->urlFor('pre');
        $urlPC=$app->urlFor('preTrPx',['id'=>'prestaionC']);
        $urlDC=$app->urlFor('preTrPx',['id'=>'prestaionD']);
        $cont=$cont.<<<End
             
                <h1>Triée les prestations</h1>
				   <ul id='trie'>
				       <li class='t'><a href='$urlP'>Prestation</a></li>
                       <li class='t'><a href='$urlPC'>Prix croissant</a></li>
                       <li class='t'><a href='$urlDC'>Prix decroissant</a></li>
                   </ul>
End;

        foreach ($this->content as $item)
        {
            //initialisation des routes qui mene a l'affichage du produit
            $url=$app->urlFor('preId',['id'=>$item->id]);
            $ajout=$app->urlFor('ajoutCoffret',['id'=>$item->id]);

            $prix="<p>".$item->prix."€</p>";
            //initialisation du contenu
            $cont=$cont.<<<End
            <img class='pic' src='$chemin/css/pic_1.png' alt=''>
            <section id=section1>
                <div class='prod'>
                    <a href='$url'><img src = '$chemin/img/$item->img' alt = '$item->img'/></a>
                </div>
            
                <div class='detail'>
                    <div>
                        <h1 class='nom' style='text-align:inherit;'><a href='$url'>$item->nom</a></h1>
                         
                    </div>

                    <div class='prix'>
                        $prix
                          <form id='form1' method="GET" action="$ajout">
                        
                        <button type='submit'>Ajouter au panier</button>
                        </form>
                </div>
            </section>
End;
        }

        $cont=$cont."<img class='pic' src='$chemin/css/pic_1.png' alt=''>";
        return $cont;
    }

    private function afficherPrestation(){

       // $Content= "<h1>".$_SESSION['panier'][0]."</h1>";

        $app= \Slim\Slim::getInstance();

        //initialisation des variable
        $item = $this->content;
        $Cat=$item->categorie;
        $prix="<p>".$item->prix."€</p>";

        //lien vers la categorie de la prestation
        $req =\giftbox\models\Categorie::where('nom','=',$Cat->nom)->first();
        $urlC= $app->urlFor('catId', ['idCat' => $req->id]);
        $ajouter=$app->urlFor('ajoutCoffret',['id'=>$item->id]);

        //initialisation du contenu
        $cont=<<<END
        <div class='prod'>
            <img src = '../web/img/$item->img' alt = '$item->img'/>
        </div>
        <div class='detail'>
            <img class='pic' src='../web/css/pic_1.png' alt=''>
            <section id=section1>
                <div class='NDCN'>
                    <p class='nom'>$item->nom</p>
                    <p>$item->descr</p>
                    <p class='nom'>Categorie : <a href='$urlC'>$Cat->nom</a></p>
                </div >
                <div class='prix'>
                    $prix
                     <form id='form1' method="GET" action="$ajouter">
                        
                        <button type='submit'>Ajouter au panier</button>
                     </form>
                </div>
            </section>
            <img class='pic' src='../web/css/pic_1.png' alt=''>
        </div>
END;
        return $cont;
    }


    private function afficherCategorie()
    {
        $app= \Slim\Slim::getInstance();
        $cont="<h1>Nos Categorie :</h1>";
        foreach ($this->content as $item){
            $urlC= $app->urlFor('catId', ['idCat' => $item->id]);
            $img=$item->nom.".jpg";
            //initialisation du contenu
            $cont=$cont.<<<END
            <div class='cat'>
            <h3><a href='$urlC'>$item->nom</a></h3>
            </div>
            
            

END;
        }
        return $cont."";

    }

    private function afficherCategoriePrestation()
    {
        $app= \Slim\Slim::getInstance();

        //initialisation des variable

        $cat = $this->content[0];
        $cat=$cat->categorie;

        //on refait une requete pour avoir les prestation d'une categorie donné
        //$pre = $cat->prestation;
        $cont= "<h1>$cat->nom:</h1>";
        $urlC= $app->urlFor('catId', ['idCat' => $cat->id]);
        $urlPC=$app->urlFor('preTrPx',['id'=>$cat->nom.'C']);
        $urlDC=$app->urlFor('preTrPx',['id'=>$cat->nom.'D']);
        $cont=$cont.<<<End
             <h1>Triée les prestations</h1>
				   <ul id='trie'>
				       <li class='t'><a href='$urlC'>Prestation</a></li>
                       <li class='t'><a href='$urlPC'>Prix croissant</a></li>
                       <li class='t'><a href='$urlDC'>Prix decroissant</a></li>
                   </ul>
End;

        foreach ($this->content as $item){
            $url=$app->urlFor('preId',['id'=>$item->id]);
            $ajouter=$app->urlFor('ajoutCoffret',['id'=>$item->id]);
            $prix="<p>".$item->prix."€</p>";
            //initialisation du contenu
            $cont=$cont.<<<END
            <img class='pic' src='../web/css/pic_1.png' alt=''>
            <section id=section1>
                <div class='prod'>
                    <a href='$url'><img src = '../web/img/$item->img' alt = '$item->img'/></a>
                </div>
            
                <div class='detail'>
                    <div>
                        <h1 class='nom' style='text-align:inherit;'><a href='$url'>$item->nom</a></h1>
                    </div>

                    <div class='prix'>
                        $prix
                         <form id='form1' method="GET" action="$ajouter">
                            <button type='submit'>Ajouter au panier</button>
                        </form>
                        </div>  
                </div>
            </section>

END;
            // <p>$item->id: $item->nom</P>
            //<p>$item->descr  $item->prix</p>
        }
        $cont=$cont."<img class='pic' src='../web/css/pic_1.png' alt=''>";
        return $cont;
    }

}

