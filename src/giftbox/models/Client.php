<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/01/2017
 * Time: 11:14
 */


namespace giftbox\models;


class Client extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'client';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function role(){
        return $this->belongsTo('\giftbox\models\Role','role_id');
    }
}