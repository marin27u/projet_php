<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 15/01/2017
 * Time: 15:25
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class Participe extends Model
{
    protected $table = 'participe   ';
    protected $primaryKey = 'id_client, id_coffret';
    public $timestamps = false;

    public function client(){
        return $this->belongsTo('\giftbox\models\Client','id_client');
    }
    public function coffret(){
        return $this->belongsTo('\giftbox\models\Coffret','id_coffret');
    }

}