<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 10/01/2017
 * Time: 18:49
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table ='role';
    protected $primaryKey='id';
    public $timestamps=false;

    public function clients(){
        return $this->hasMany('\giftbox\models\Client','role_id');
    }
}