<?php
namespace giftbox\models;
class Prestation extends \Illuminate\Database\Eloquent\Model{

	protected $table ='prestation';
	protected $primaryKey='id';
	public $timestamps=false;

	//Méthode qui retourne la categorie d'une prestation
	public function categorie(){
		return $this->belongsTo('\giftbox\models\Categorie','cat_id');
	} 
}