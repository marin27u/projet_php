<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/01/2017
 * Time: 11:14
 */


namespace giftbox\models;


class Contient extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'contient';
    protected $primaryKey = 'IdCoffret, IdPrestation';
    public $timestamps = false;

    public function coffret(){
        return $this->belongsTo('\giftbox\models\Coffret','IdCoffret');
    }
    public function prestation(){
        return $this->belongsTo('\giftbox\models\Prestation','IdPrestation');
    }
}