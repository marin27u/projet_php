<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/01/2017
 * Time: 11:14
 */


namespace giftbox\models;


class Coffret extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'coffret';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function createur(){
        return $this->belongsTo('\giftbox\models\Client','idCreateur');
    }

//    public function participants(){
//        return $this->hasMany('\giftbox\models\Participe','id_client');
//    }
}