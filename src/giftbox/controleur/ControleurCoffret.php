<?php


namespace giftbox\Controleur;

use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\models\Coffret;
use giftbox\models\Contient;
use giftbox\models\Client;

use giftbox\utils\Authentification;
use giftbox\utils\AuthException;
use giftbox\Vue\VueAccueil;
use giftbox\Vue\VueCoffret;
use Slim\Slim;

class ControleurCoffret
{

    //Méthode qui ajoute une prestation au coffret
    public function ajoutCoffret($id)
    {
        $prestation = Prestation::where('id', '=', $id)->first();
        if ($prestation != null) {

            if (isset($_SESSION['panier'][$id])){
                $_SESSION['panier'][$id] +=1;
                $this->calculerCoffret();

            }


            else{
                $_SESSION['panier'][$id] = 1;
                $this->calculerCoffret();
            }


        }
        $app = Slim::getInstance();
        $app->redirect($app->urlFor('listerCoffret'));
    }

    public function listerCoffret()
    {
        $tab = [];
        if (empty($_SESSION['panier'])){

        }
        else {
            foreach ($_SESSION['panier'] as $idPrest => $value) {
                $prestation = Prestation::where('id', '=', $idPrest)->first();
                $tab[] = ["quantite"=>$value,"prestation"=>$prestation];
            }
        }


        $vue=new VueCoffret($tab);
        $html=$vue->render(1);

        echo $html;


    }

    public function suppCoffret($id)
    {
        if ($_SESSION['panier'][$id] != null) {
            $prestation = Prestation::where('id', '=', $id)->first();

            if ($prestation != null) {

                if(isset($_SESSION['panier'][$id])){
                    $_SESSION['panier'][$id] -=1;

                    if($_SESSION['panier'][$id] <= 0)
                        unset($_SESSION['panier'][$id]);

                }


                $this->calculerCoffret();
            }
            $app = Slim::getInstance();
            $app->redirect($app->urlFor('listerCoffret'));
        }
    }

    private function calculerCoffret(){
        $_SESSION['prixPanier'] = 0;
        foreach ($_SESSION['panier'] as $idPrest => $qte){
            $prestation = Prestation::where('id','=',$idPrest)->first();
            $_SESSION['prixPanier'] += $prestation->prix * $qte;
        }
    }

    public function connexion(){
        $vue=new VueAccueil();
        $html=$vue->render(2);
        echo $html;
    }
    public function inscription(){
        $vue=new VueAccueil();
        $html=$vue->render(3);
        echo $html;
    }




    function sinscrire(){

        $app = Slim::getInstance();

        $email = $app->request->post('email');
        $pass = $app->request->post('password');
        $confirm = $app->request->post('confirm');

        try{
            Authentification::createUser($email,$pass,$confirm);
        }catch (AuthException $ae){
            $app->redirect($app->urlFor('inscription'));
        }
        $app->redirect($app->urlFor('accueil'));

    }

    function seConnecter(){
        $app = Slim::getInstance();

        $email = $app->request->post('email');
        $pass = $app->request->post('password');

        try{
            Authentification::authenticate($email,$pass);
            //ICI POUR LA REDIRECTION MONCOMPTE
            $app->redirect($app->urlFor('accueil'));
        }
        catch (AuthException $ae){
            $app->redirect($app->urlFor('connexion'));
        }

    }

    public function validation(){
        $app = Slim::getInstance();
        $nbP=0;
        $nbC=[];
        foreach ($_SESSION['panier'] as $idPrest => $qte){
            $nbP++;
            $prestationCAt=Prestation::where('id','=',$idPrest)->first();
            $prestationCAt=$prestationCAt->categorie()->first();
            if(!isset($nbC[$prestationCAt->nom])) {
                $nbC[$prestationCAt->nom] = 1;
            }
        }
        $vue=new VueAccueil();
        if(isset($_SESSION['profile'])) {
            if (($nbP >= 2) && (count($nbC) >= 2)) {
                $html = $vue->render(4);
            } else {
                $html = $vue->render(5);
            }
        }else{
            $app->redirect($app->urlFor('connexion'));
        }


        echo $html;
    }

    public function recapCoffret(){
        $Profile=$_SESSION['profile'];
        $cof=new Coffret();
        $cof->code="PasDefinie";
        $cof->etat="valider";
        $cof->paye=false;
        $cof->password="rien";
        $cof->idCreateur=$Profile['userid'];
        $cof->message="message";
        $cof->prix=$_SESSION['prixPanier'];
        $cof->save();

        $app = Slim::getInstance();
        $url=$cof->id;
        $cof->code=hash("md5",$url);
        $cof->save();

        $_SESSION["coffret"]=$cof->code;
        $message = $app->request->post('message');
        //echo $message;
        $cof->message=$message;
        $cof->save();

        $tab = [];
        foreach ($_SESSION['panier'] as $idPrest => $value) {
            $prestation = Prestation::where('id', '=', $idPrest)->first();
            $tab[] = ["quantite"=>$value,"prestation"=>$prestation];
        }
        foreach ($_SESSION['panier'] as $idPrest => $value) {
            $Con=new Contient();
            $Con->IdCoffret=$cof->id;
            $Con->IdPrestation=$idPrest;
            $Con->quantite=$value;
            $Con->save();
        }
        unset($_SESSION['panier']);
        $vue=new VueCoffret($tab);
        $html=$vue->render(2);
        unset($_SESSION['panier']);
        unset($_SESSION['prixPanier']);
        echo $html;
    }

    public function Payement(){
        $Profile=$_SESSION['profile'];
        $Profile=$Profile['userid'];
        $cof=Coffret::where("idCreateur",'=',$Profile)->where("code","=",$_SESSION["coffret"])->first();
        $cof->etat="payé";
        $cof->save();

        $vue=new VueAccueil();
        $html=$vue->render(6);
        echo $html;
    }

    public function CreeURl(){
        $Profile=$_SESSION['profile'];
        $Profile=$Profile['userid'];
        $cof=Coffret::where("idCreateur",'=',$Profile)->where("code","=",$_SESSION["coffret"])->first();
        /*
        foreach ($_SESSION['panier'] as $idPrest => $value) {
            $Con=new Contient();
            $Con->IdCoffret=$cof->id;
            $Con->IdPrestation=$idPrest;
            $Con->quantite=$value;
            $Con->save();
        }

        unset($_SESSION['panier']);
        unset($_SESSION['prixPanier']);
        */
        $app = Slim::getInstance();
        $app->redirect($app->urlFor('CoffretUrl',['id'=>$cof->code]));
    }

    public function CoffretUrl($id)
    {

        $tab = [];

        $coffret = Coffret::where('code', '=', $id)->first();

        if ($coffret->etat == "payé") {
            $contient = Contient::where('IdCoffret', '=', $coffret->id)->get();
            foreach ($contient as $con) {
                $pre = $con->prestation;
                $tab[] = ["quantite" => $con->quantite, "prestation" => $pre];

            }

            $tab["message"]=$coffret->message;

            $vue = new VueCoffret($tab);
            $html = $vue->render(3);
        }else {
            $app = Slim::getInstance();
            $_SESSION["coffret"]=$id;
            $urlP=$app->urlFor('Payement');
            $html = "erreur :le coffret n'est pas payé";
            $html = $html . <<<End
                        <form id='form1' method='GET' action="$urlP">
                            <button type='submit'>Payez le</button>
                        </form>
End;

        }

        echo $html;
    }
    public function MonCompte(){
        $Profile=$_SESSION['profile'];
        $Profile=$Profile['userid'];
        $client=Coffret::where('idCreateur','=',$Profile)->orderBy('id', 'DESC')->get();

        $vue = new VueCoffret($client);
        $html = $vue->render(4);
        echo $html;
    }
    public function deconnexion(){
        unset($_SESSION['profile']);
        $app = Slim::getInstance();
        $app->redirect($app->urlFor("connexion"));
    }

}