<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 17/01/2017
 * Time: 16:57
 */

namespace giftbox\Controleur;


use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\utils\Authentification;
use giftbox\utils\AuthException;
use Slim\Slim;

class ControleurAdmin
{
    public function admin(){
        try {
            Authentification::checkAccessRights(1);
            $app = Slim::getInstance();
            $urlAdd = $app->urlFor('adminAjoutPre');
            $urlDel = $app->urlFor('adminSuppPre');
            $html = <<<END
         <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <title>GiftBox</title>
                    <meta charset="utf-8">
		            <link rel="stylesheet">
                </head>
                <body>
                <a href = $urlAdd>Ajouter</a>
                <a href = $urlDel>Supprimer</a>
	            </body>
            </html>
END;
            echo $html;
        } catch (AuthException $ae) {
            echo $ae->getMessage();
        }

    }


    /*Charge la page d'ajout de prestation*/
    public function ajoutPrestation()
    {
        try {
            Authentification::checkAccessRights(1);
            $app = Slim::getInstance();
            $url = $app->urlFor('ajouterPrestation');
            $html = <<<END
         <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <title>GiftBox</title>
                    <meta charset="utf-8">
		            <link rel="stylesheet">
                </head>
                <body>
                <form enctype="multipart/form-data" id='form1' method='POST' action="$url">
                        <input name='nom' placeholder = 'nom'>
                        <input name='descr' placeholder = 'description'>
                        <input name='cat' placeholder = 'catégorie'>
                        <input name='prix' placeholder = 'prix'>
                        <input type = 'file' name='img' placeholder = 'img' accept="image/*">                       
                        <button type='submit'>ajouter</button>
                        </form>
	            </body>
            </html>
END;
            echo $html;
        } catch (AuthException $ae) {
            echo $ae->getTrace();
        }
    }

    /* COrrespond au bouton ajouter */
    public function ajouterPrestation()
    {
        try {
            Authentification::checkAccessRights(1);
            $app = Slim::getInstance();

            $nom = filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);

            $descr = filter_var($app->request->post('descr'), FILTER_SANITIZE_STRING);

            $nomCat = filter_var($app->request->post('cat'), FILTER_SANITIZE_STRING);

            $prix = filter_var($app->request->post('prix'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            echo $prix;


            if (!(empty($nom) || empty($descr) || empty($nomCat) || empty($prix))) {

                $cat = Categorie::where('nom', '=', $nomCat)->first();

                if (isset($cat)) {

                    /* Récupère l'image et la place dans le dossier img*/
                    $uploaddir = "web/img/";
                    $uploadfile = $uploaddir . basename($_FILES['img']['name']);
                    if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {

                        /*Récupère le nom de l'image*/
                        $img = $_FILES['img']['name'];
                        $prestation = new Prestation();
                        $prestation->nom = $nom;
                        $prestation->descr = $descr;
                        $prestation->img = $img;
                        $prestation->prix = $prix;
                        $prestation->cat_id = $cat->id;
                        $prestation->save();
                        echo "Insertion terminé";
                    }

                } else {
                    echo " erreur";
                }

            } else {
                echo "erreur";
            }

        } catch (AuthException $ae) {
            echo $ae->getTrace();
        }
    }


    public function suppPrestation()
    {
        try {
            Authentification::checkAccessRights(1);
            $content = $this->listerPrestations();
            $html = <<<END
         <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <title>GiftBox</title>
                    <meta charset="utf-8">
		            <link rel="stylesheet">
                </head>
                <body>
                $content
	            </body>
            </html>
END;
            echo $html;
        } catch (AuthException $ae) {
            echo $ae->getTrace();
        }

    }

    public function supprimerPrestation($id)
    {
        $app = Slim::getInstance();

        $prestation = Prestation::where('id','=',$id)->first();
        $prestation->delete();

        $app->redirect($app->urlFor('adminSuppPre'));
        echo "$id  supprimé";
    }


    private function listerPrestations()
    {
        $app = Slim::getInstance();
        $content = '';
        $prestations = Prestation::all();
        foreach ($prestations as $prestation) {
            $url = $app->urlFor('suppPrestation', ['id' => $prestation->id]);
            $nom = $prestation->nom;
            $descr = $prestation->descr;
            $content .= <<<END
                <form id='form1' method='POST' action="$url">
                    <p>$nom</p>
                    <p>$descr</p>
                    <button type='submit'>supprimer</button>
                </form>
END;
        }
        return $content;
    }
}