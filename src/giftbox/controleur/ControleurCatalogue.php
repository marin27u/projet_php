<?php

namespace giftbox\Controleur;


use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\Vue\VueCatalogue;
use giftbox\Vue\VueAccueil;

class ControleurCatalogue  {

    //Méthode qui definit l'accueil du site giftBox
    function accueil(){
        $vue=new VueAccueil();
        $html=$vue->render(1);
        echo $html;
    }


    //Méthode qui definit l'affichage des categories .Elle a un parametre qui represente l'id de la categorie
    // et s'il est null on affiche l'ensemble des categories
    function cat($id = null){

        if ($id == null){

            //on fait une requette pour afficher l'ensemble des categories
            $req = \giftbox\models\Categorie::select()->get();

            $vue = new VueCatalogue($req);

            $Html=$vue->render(3);
            echo $Html;
        }

        else{

            //on fait une requette pour affiche les presations relié à cette categorie
            $this->categoriePrestation($id);
        }



    }

    //Méthode qui affiche une presation ou l'ensemble des prestations. elle a un parametre
    //et s'il est null on affiche l'ensemble des prestations.
    //Sinon on affiche la prestation à qui apartient l'id passé en parametre
    public function prestation($id = null) {
        if ($id == null) {

            //on fait une requette pour affiche l'ensemble des presations
            $req = Prestation::all ();

            $vue = new VueCatalogue($req);
            $Html=$vue->render(1);
            echo $Html;

        } else {

            //on fait une requette pour affiche la presation
            $req = Prestation::where ( 'id', '=', $id )->first ();
            $vue = new VueCatalogue($req);
            $Html=$vue->render(2);
            echo $Html;
        }
    }

    //Méthode qui affiche les presations relié à une categorie,
    //l'id de la categorie est passé en parametre
    private function categoriePrestation($id)
    {
        $req =Categorie::where('id','=',$id)->first();
        $req=$req->prestation()->get();

        $vue = new VueCatalogue($req);

        $Html=$vue->render(4);
        echo $Html;

    }
    public function prestaionTrieePrix($id){
        $a=0;
        switch ($id) {
            case 'prestaionC' :
                $a = 5;
                $req = Prestation::select()->orderBy('prix')->get();
                break;
            case 'prestaionD' :
                $a = 5;
                $req = Prestation::select()->orderBy('prix', 'DESC')->get();
                break;
                $req=Categorie::select();

            case 'AttentionC' :
                $a = 6;
                $req =Categorie::where('id','=',1)->first();
                $req=$req->prestation()->orderBy('prix')->get();
                break;
            case 'AttentionD' :
                $a = 6;
                $req =Categorie::where('id','=',1)->first();
                $req=$req->prestation()->orderBy('prix','DESC')->get();
                break;
            case 'ActivitéC' :
                $a = 6;
                $req =Categorie::where('id','=',2)->first();
                $req=$req->prestation()->orderBy('prix')->get();
                break;
            case 'ActivitéD' :
                $a = 6;
                $req =Categorie::where('id','=',2)->first();
                $req=$req->prestation()->orderBy('prix','DESC')->get();
                break;
            case 'RestaurationC' :
                $a = 6;
                $req =Categorie::where('id','=',3)->first();
                $req=$req->prestation()->orderBy('prix')->get();
                break;
            case 'RestaurationD' :
                $a = 6;
                $req =Categorie::where('id','=',3)->first();
                $req=$req->prestation()->orderBy('prix','DESC')->get();
                break;
            case 'HébergementC' :
                $a = 6;
                $req =Categorie::where('id','=',4)->first();
                $req=$req->prestation()->orderBy('prix')->get();
                break;
            case 'HébergementD' :
                $a = 6;
                $req =Categorie::where('id','=',4)->first();
                $req=$req->prestation()->orderBy('prix','DESC')->get();
                break;
        }
        $vue = new VueCatalogue($req);
        $Html=$vue->render($a);
        echo $Html;

    }


}