<?php
require 'vendor/autoload.php';
use \giftbox\controleur\ControleurCoffret;
use \giftbox\controleur\ControleurCatalogue;
use \giftbox\models\categorie;
use \giftbox\models\prestation;
use giftbox\Controleur\ControleurAdmin;

//conection a la base de donnée
$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

//connection a FrameWork
$app=new \Slim\Slim;

session_start();


//initialisation du coffret
if (!isset($_SESSION['panier']))
    $_SESSION['panier'] = [];

//initialisation du prix du coffret à 0
if (!isset($_SESSION['prixPanier']))
    $_SESSION['prixPanier'] = 0;


//chose utile pour la gestion du coffret
//$_SESSION['panier'][]=;
//unset($_SESSION['prixPanier']);

$app->get('/',function(){(new ControleurCatalogue())->accueil();})->name('accueil');

$app->get('/cat',function (){(new ControleurCatalogue())->cat();})->name('cat');

$app->get('/cat/:idCat',function ($idCat){(new ControleurCatalogue())->cat($idCat);})->name('catId');

$app->get('/preTriePx/:id',function ($id){(new ControleurCatalogue())->prestaionTrieePrix($id);})->name('preTrPx');

$app->get('/pre',function (){(new ControleurCatalogue())->prestation();})->name('pre');

$app->get('/ajoutCoffret/:id',function($id){(new ControleurCoffret())->ajoutCoffret($id);})->name('ajoutCoffret');

$app->get('/suppCoffret/:id',function($id){(new ControleurCoffret())->suppCoffret($id);})->name('suppCoffret');

$app->get('/coffret',function(){(new ControleurCoffret())->listerCoffret();})->name("listerCoffret");

$app->get('/pre/:id',function($id){(new ControleurCatalogue())->prestation($id);})->name('preId');

$app->post('/seConnecter',function(){(new ControleurCoffret())->seConnecter();})->name("seCo");

$app->get('/connexion',function(){(new ControleurCoffret())->connexion();})->name("connexion");

$app->get('/deconnexion',function(){(new ControleurCoffret())->deconnexion();})->name("deconnexion");

$app->get('/inscription',function(){(new controleurCoffret())->inscription();})->name("inscription");

$app->post('/sinscrire',function(){(new controleurCoffret())->sinscrire();})->name("sinscrire");

$app->get('/validation',function(){(new ControleurCoffret())->validation();})->name('validation');

$app->post('/recapCoffret',function(){(new ControleurCoffret())->recapCoffret();})->name('recapCoffret');

$app->get('/Payement',function(){(new ControleurCoffret())->Payement();})->name('Payement');

$app->get('/CreeUrl',function(){(new ControleurCoffret())->CreeURl();})->name('CreeUrl');

$app->get('/Coffret/:id',function($id){(new ControleurCoffret())->CoffretUrl($id);})->name('CoffretUrl');

$app->get('/MonCompte',function(){(new ControleurCoffret())->MonCompte();})->name('MonCompte');

$app->get('/admin',function (){(new ControleurAdmin())->admin();})->name('admin');

$app->get('/admin/ajoutPre',function (){(new ControleurAdmin())->ajoutPrestation();})->name('adminAjoutPre');
$app->post('/admin/ajouterPrestation',function (){(new ControleurAdmin())->ajouterPrestation();})->name('ajouterPrestation');

$app->get('/admin/suppPre',function (){(new ControleurAdmin())->suppPrestation();})->name('adminSuppPre');
$app->post('/admin/suppPrestation/:id',function ($id){(new ControleurAdmin())->supprimerPrestation($id);})->name('suppPrestation');

$app->run();
